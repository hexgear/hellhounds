# Hellhounds of the Cosmos

[![Creative Commons Zero v1.0 Universal][CC0-1.0-badge]](COPYING)
[![Created with Ren'Py][renpy-badge]][renpy-url]

By [Clifford D. Simak][simak].

> Weird are the conditions of the
> interdimensional struggle faced
> by Dr. White's ninety-nine men.

[![Hell Hound][hound]][hound-url]

This image is attributed to the [Ryzom MMORPG][ryzom] and is licensed under a [Creative Commons Attribution 2.0 Generic License][CC-BY-SA-2.0].

## Dependencies

- [Git](https://git-scm.com) (optional)

- [The Ren'Py Visual Novel Engine](https://www.renpy.org)

## Getting Started

### Sources

Grab a copy of the source code by cloning this repository via the following command:

```sh
git clone https://gitlab.com/hexgear/hellhounds.git
```

-Or-

[Click here to download the latest code](https://gitlab.com/hexgear/hellhounds/-/archive/master/hellhounds-master.zip).

### Opening in Ren'Py Launcher

Copy the source code under the Ren'Py project directory.

![Ren'Py Launcher](docs/img/renpy_launcher.png)

**Note: the inner project directory MUST be titled `game`, otherwise Ren'Py's launcher will not recognize it as a project.**

## License

This work is licensed under a [Creative Commons Zero v1.0 Universal License](COPYING).

## Reference

- [Quickstart - Ren'Py Documentation](https://www.renpy.org/doc/html/quickstart.html)

[CC0-1.0-badge]: https://img.shields.io/badge/license-CC0--1.0-black.svg?style=flat-square&logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+DQo8c3ZnIGlkPSJzdmcyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNjQiIHdpZHRoPSI2NCIgdmVyc2lvbj0iMS4wIj4NCiA8cGF0aCBkPSJtMzIgMTMuNThjLTEwLjU2NCAwLTEzLjIyIDkuOTctMTMuMjIgMTguNDItMC4wMDIgOC40NTIgMi42NiAxOC40MiAxMy4yMiAxOC40MiAxMC41NjUgMCAxMy4yMi05Ljk3IDEzLjIyLTE4LjQycy0yLjY1NS0xOC40Mi0xMy4yMi0xOC40MnptMCA2Ljk1YzAuNDMgMCAwLjgyIDAuMDYgMS4xOSAwLjE1IDAuNzYgMC42NiAxLjEzIDEuNTY0IDAuNCAyLjgzbC03LjAzNCAxMi45MjZjLTAuMjE2LTEuNjM2LTAuMjQ2LTMuMjQtMC4yNDYtNC40MzYgMC0zLjcyMyAwLjI1Ny0xMS40NzQgNS42OS0xMS40NzR6bTUuMjY3IDUuOTU3YzAuNDMzIDEuOTgzIDAuNDMzIDQuMDU2IDAuNDMzIDUuNTEzIDAgMy43Mi0wLjI2IDExLjQ3NS01LjcgMTEuNDc1LTAuNDI1IDAtMC44Mi0wLjA0NS0xLjE4NS0wLjEzNS0wLjA3NS0wLjAyMi0wLjEzNS0wLjA0LTAuMjA1LTAuMDctMC4xMS0wLjAzLTAuMjMtMC4wNy0wLjMzMy0wLjExLTEuMjEtMC41MTMtMS45NzItMS40NDQtMC44NzctMy4wOWw3Ljg2Ny0xMy41OHoiLz4NCiA8cGF0aCBkPSJtMzEuOTMzIDBjLTguODczIDAtMTYuMzU5IDMuMDktMjIuNDUzIDkuMy0zLjA5IDMuMDktNS40NDQgNi42LTcuMDggMTAuNTMtMS42IDMuODktMi40IDcuOTQtMi40IDEyLjE3IDAgNC4yNyAwLjggOC4zMiAyLjQgMTIuMTdzMy45MiA3LjMxIDYuOTcgMTAuNGMzLjA4IDMuMDQgNi41NDUgNS4zOSAxMC4zOSA3LjAzIDMuODkgMS42IDcuOTQgMi40IDEyLjE3IDIuNHM4LjM0LTAuODMgMTIuMzEtMi40NmMzLjk2LTEuNjQgNy40OS00IDEwLjYyLTcuMDkgMy4wMS0yLjkzIDUuMjktNi4yOTMgNi44MS0xMC4xIDEuNTYtMy44NSAyLjMzLTcuOTcgMi4zMy0xMi4zNSAwLTQuMzQtMC43Ny04LjQ1LTIuMzMtMTIuMy0xLjU2Mi0zLjg4OC0zLjg1LTcuMzIzLTYuODYtMTAuMzMzLTYuMjg1LTYuMjQ3LTEzLjkyLTkuMzY3LTIyLjg4LTkuMzY3em0wLjEzNCA1Ljc2YzcuMjM4IDAgMTMuNDEzIDIuNTcgMTguNTUzIDcuNzEgMi40OCAyLjQ4IDQuMzggNS4zMDggNS42NzEgOC40NyAxLjI5OSAzLjE2IDEuOTQ5IDYuNTQgMS45NDkgMTAuMDYgMCA3LjM1LTIuNTE1IDEzLjQ1LTcuNTEgMTguMzMtMi41OSAyLjUyLTUuNSA0LjQ0OC04LjczIDUuNzgtMy4yMSAxLjM0LTYuNSAxLjk5Ni05LjkzMyAxLjk5Ni0zLjA2NyAwLTYuNzg4LTAuNjUzLTkuOTQ5LTEuOTQ2LTMuMTU4LTEuMzM2LTYuMDAxLTMuMjQtOC41MTgtNS43Mi0yLjUxMy0yLjUxLTQuNDUtNS4zNS01LjgyNC04LjUxLTEuMzM2LTMuMi0yLjAxNi02LjUtMi4wMTYtOS45MyAwLTMuNDcgMC42OC02Ljc5IDIuMDItOS45NSAxLjM3LTMuMiAzLjMxLTYuMDc1IDUuODItOC42MyA0Ljk5LTUuMDMgMTEuMTUtNy42NiAxOC40NjctNy42NnoiLz4NCjwvc3ZnPg==
[CC-BY-SA-2.0]: https://creativecommons.org/licenses/by-sa/2.0/
[renpy-badge]: https://img.shields.io/badge/powered--by-ren'py-%23ff7f7f.svg?style=flat-square
[renpy-url]: https://www.renpy.org
[hound]: docs/img/hound.jpg
[hound-url]: https://flic.kr/p/o9RfTV
[ryzom]: https://ryzom.com
[simak]: https://en.wikipedia.org/wiki/Clifford_D._Simak
