FROM runmymind/docker-android-sdk:ubuntu-standalone

WORKDIR /opt/hellhounds
COPY . /opt/hellhounds

RUN apt-get install -y renpy
